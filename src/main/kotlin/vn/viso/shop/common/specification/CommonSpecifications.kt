package vn.viso.shop.common.specification

import org.springframework.data.jpa.domain.Specification
import vn.viso.shop.common.enum.status.CommonStatus
import vn.viso.shop.common.enum.type.OrderByType
import javax.persistence.criteria.Order

fun <T> initSpec(): Specification<T> {
    return Specification { _, _, _ -> null }
}

fun <T> hasStatus(activeStatus: Int = CommonStatus.ACTIVE): Specification<T> {
    return Specification { root, _, criteriaBuilder ->
        criteriaBuilder.equal(root.get<Any>("status"), activeStatus)
    }
}

fun <Root, Relation> fetchRelation(relationName: String): Specification<Root> {
    return Specification { root, query, criteriaBuilder ->
        query.distinct(true)
        root.fetch<Root, Relation>(relationName)
        criteriaBuilder.conjunction()
    }
}

fun <Root> fetchRelations(relationNames: List<String>): Specification<Root> {
    return Specification { root, query, criteriaBuilder ->
        query.distinct(true)
        for (relation in relationNames) {
            root.fetch<Root, Any>(relation)
        }
        criteriaBuilder.conjunction()
    }
}

fun <Root> distinctRootEntity(): Specification<Root> {
    return Specification { _, query, _ ->
        query.distinct(true)
        null
    }
}

fun <T> orderBy(field: String, orderByType: OrderByType = OrderByType.ASC): Specification<T> {
    return Specification<T> { root, query, criteriaBuilder ->
        val path = root.get<Any>(field)
        query.orderBy(if (orderByType == OrderByType.ASC) {
            criteriaBuilder.asc(path)
        } else {
            criteriaBuilder.desc(path)
        })
        null
    }
}

fun <T> orderBy(orderByOptions: Map<String, OrderByType>): Specification<T> {
    return Specification<T> { root, query, criteriaBuilder ->
        val orders = mutableListOf<Order>()

        orderByOptions.entries.forEach {
            val path = root.get<Any>(it.key)
            orders.add(if (it.value == OrderByType.ASC) {
                criteriaBuilder.asc(path)
            } else {
                criteriaBuilder.desc(path)
            })
        }
        query.orderBy(orders)
        null
    }
}
