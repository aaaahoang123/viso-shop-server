package vn.viso.shop.common.enum.status

object CommonStatus {
    const val ACTIVE = 1
    const val INACTIVE = -1
}