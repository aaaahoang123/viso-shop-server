package vn.viso.shop.common.enum.type

enum class OrderByType {
    ASC, DESC
}