package vn.viso.shop.common.enum.type

object Gender {
    const val MALE = 1
    const val FEMALE = 2
    const val OTHER = 3
}