package vn.viso.shop.libs.enum

import java.lang.reflect.Field
import java.lang.reflect.Modifier

open class CustomEnum {
    companion object {
        private lateinit var reserveMap: Map<Any, String>

        fun getAllStaticField(): List<Field> {
            val fields: Array<Field> = this::class.java.declaredFields
            val result = mutableListOf<Field>()
            for (f in fields) {
                if (Modifier.isStatic(f.modifiers)) {
                    result.add(f)
                }
            }
            return result
        }

        fun getDescription(value: Any): String? {
            return reserveMap[value]
        }
    }

    init {
        val fields = getAllStaticField()
        val tempMap = mutableMapOf<Any, String>()
        for (field in fields) {
            field.isAccessible = true
            tempMap[field.get(this)] = field.name
        }
        reserveMap = tempMap
    }

}