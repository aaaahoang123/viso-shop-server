package vn.viso.shop.libs.auth

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import io.jsonwebtoken.*
import net.bytebuddy.utility.RandomString
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.crypto.password.PasswordEncoder
import vn.viso.shop.common.utils.assignObject
import vn.viso.shop.entity.User
import vn.viso.shop.libs.exception.ExecuteException
import vn.viso.shop.repository.user.UserRepository
import vn.viso.shop.repository.user.userEmailEqual
import vn.viso.shop.request.AuthRequest


@Service
class AuthServiceImpl @Autowired constructor(
        private val userRepo: UserRepository,
        private val passwordEncoder: PasswordEncoder
//        private val userRoleRepository: UserRoleRepository
) : AuthService {
    @Value("\${jwt.secret:adjkfhasf897235hjgasfdghjsat612dfas}")
    private lateinit var _jwtSecret: String
    @Value("\${jwt.ttl:604800000}")
    private var _jwtExpiration: Long = 0

    override fun register(dto: AuthRequest): User {
        if (userRepo.count(userEmailEqual(dto.email!!)) > 0) {
            throw ExecuteException("duplicate_email")
        }
        dto.password = passwordEncoder.encode(dto.password)
        val member = assignObject(User(), dto)
        member.id = generateId()
        return userRepo.save(member)
    }

    override fun attemptMember(email: String, password: String): User {
        val oMember = userRepo.findMemberByEmail(email)
        if (!oMember.isPresent) {
            throw ExecuteException("account_not_exist")
        }
        val member = oMember.get()
        if (!passwordEncoder.matches(password, member.password)) {
            throw ExecuteException("wrong_password")
        }
        return member
    }

    override fun generateToken(user: User): String {
        val now = Date()
        val expiryDate = Date(now.time + _jwtExpiration)
        // Tạo chuỗi json web token từ id của user.
        return Jwts.builder()
                .setSubject(user.id)
                .setIssuedAt(now)
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS512, _jwtSecret)
                .compact()
    }

    @Throws(exceptionClasses = [MalformedJwtException::class, ExpiredJwtException::class, UnsupportedJwtException::class, IllegalArgumentException::class])
    override fun decodeToken(token: String): User {
        val claims = Jwts.parser()
                .setSigningKey(_jwtSecret)
                .parseClaimsJws(token)
                .body
                .subject
//        return userRepo.findMemberByIdAndFetchPolicy(id)
        return userRepo.findById(claims).get()
    }

    private fun generateId(): String {
        val result = RandomString.make(6)
        if (userRepo.existsById(result)) {
            return generateId()
        }
        return result
    }
}