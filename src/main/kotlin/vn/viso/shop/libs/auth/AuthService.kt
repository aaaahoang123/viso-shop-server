package vn.viso.shop.libs.auth

import vn.viso.shop.entity.User
import vn.viso.shop.request.AuthRequest
import java.util.*

interface AuthService {
    fun register(dto: AuthRequest): User
    fun attemptMember(email: String, password: String): User
    fun generateToken(user: User): String
    fun decodeToken(token: String): User
}