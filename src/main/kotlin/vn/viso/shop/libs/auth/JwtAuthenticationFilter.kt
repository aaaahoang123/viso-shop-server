package vn.viso.shop.libs.auth

import org.springframework.http.HttpHeaders.AUTHORIZATION
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter
import org.springframework.security.web.util.matcher.RequestMatcher
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class JwtAuthenticationFilter(
        requestMatcher: RequestMatcher,
        manager: AuthenticationManager,
        private val authService: AuthService
): AbstractAuthenticationProcessingFilter(requestMatcher) {
    init {
        authenticationManager = manager
    }

    override fun attemptAuthentication(request: HttpServletRequest, response: HttpServletResponse): Authentication {
        var token = request.getHeader(AUTHORIZATION)?.trim()
        if (token?.startsWith("Bearer ") != true) {
            throw BadCredentialsException("token_is_not_a_bearer_token")
        }
        token = token.replace("Bearer ", "")
        val user = try {
            val u = authService.decodeToken(token)
            request.setAttribute(USER_ATTR_NAME, u)
            u
        } catch (e: Exception) {
            if (logger.isDebugEnabled) {
                logger.debug("Decode the token: $token failed, reason: " + e.message)
            }
            null
        }

        return authenticationManager.authenticate(UsernamePasswordAuthenticationToken(user, user))
    }

    override fun successfulAuthentication(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain, authResult: Authentication) {
        SecurityContextHolder.getContext().authentication = authResult
        chain.doFilter(request, response)
    }
}
