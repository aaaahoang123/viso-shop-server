package vn.viso.shop.libs.auth

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.builders.WebSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.web.util.matcher.AntPathRequestMatcher
import org.springframework.security.web.authentication.AnonymousAuthenticationFilter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.http.HttpStatus
import org.springframework.security.web.authentication.HttpStatusEntryPoint
import org.springframework.security.web.AuthenticationEntryPoint
import org.springframework.transaction.annotation.EnableTransactionManagement
import vn.viso.shop.API_PREFIX

@Configuration
@EnableWebSecurity
@EnableTransactionManagement(proxyTargetClass=true)
@EnableGlobalMethodSecurity(jsr250Enabled = true, securedEnabled = true)
class WebSecurityConfig @Autowired constructor(
    private val authService: AuthService
) : WebSecurityConfigurerAdapter() {

    @Bean
    fun forbiddenEntryPoint(): AuthenticationEntryPoint {
        return HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED)
    }

    override fun configure(http: HttpSecurity) {
        //                .antMatchers("/",
//                "/favicon.ico",
//                "/**/*.png",
//                "/**/*.gif",
//                "/**/*.svg",
//                "/**/*.jpg",
//                "/**/*.html",
//                "/**/*.css",
//                "/**/*.js")
//                .permitAll()
        http.sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .exceptionHandling()
                .accessDeniedHandler(CustomAccessDeniedHandler())
                .and()
                .authorizeRequests()
                .antMatchers("/api/auth/login**", "/api/auth/register**", "/api/guest/**")
                .permitAll()
                .antMatchers("/api/**")
                .authenticated()
                .and()
                .cors()
                .and()
                .csrf().disable()
                .formLogin().disable()
                .httpBasic().disable()
                .logout().disable()

//        return filter
        http
            .addFilterBefore(
                JwtAuthenticationFilter(AntPathRequestMatcher("$API_PREFIX/v1/**"), authenticationManager(), authService),
                AnonymousAuthenticationFilter::class.java
            )
            .addFilterBefore(
                JwtAuthenticationFilter(AntPathRequestMatcher("$API_PREFIX/admin/**"), authenticationManager(), authService),
                AnonymousAuthenticationFilter::class.java
            )
    }

    override fun configure(web: WebSecurity) {
        web.ignoring()
                .antMatchers( "/upload/**", "/upload")
    }
}
