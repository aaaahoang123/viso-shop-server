package vn.viso.shop.libs.auth

import org.springframework.context.annotation.Configuration
import org.springframework.web.method.support.HandlerMethodArgumentResolver
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import vn.viso.shop.libs.auth.ReqUserResolver


@Configuration
class ReqUserAnnotationConfig : WebMvcConfigurer {
    override fun addArgumentResolvers(argumentResolvers: MutableList<HandlerMethodArgumentResolver>) {
        argumentResolvers.add(ReqUserResolver())
    }
}
