package vn.viso.shop.libs.locale

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.MessageSource
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.support.ReloadableResourceBundleMessageSource
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean
import org.springframework.web.servlet.LocaleResolver
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor
import java.util.*
import javax.validation.Validator


@Configuration
class LocaleConfig : WebMvcConfigurer {
    private var messageSource: MessageSource? = null

    private var validator: Validator? = null
    @Bean
    fun validatorFactory(messageSource: MessageSource?): Validator {
        if (validator == null) {
            val tempValidator = LocalValidatorFactoryBean()
            tempValidator.setValidationMessageSource(messageSource!!)

            validator = tempValidator;
        }
        return validator!!
    }

    @Bean("messageSource")
    fun messageSource(): MessageSource {
        if (messageSource == null) {
            val tempMessageSource = ReloadableResourceBundleMessageSource()
            tempMessageSource.addBasenames(
                    "classpath:locale/messages",
                    "classpath:locale/enums",
                    "classpath:locale/ValidationMessages",
                    "classpath:org/hibernate/validator/ValidationMessages"
            )
            tempMessageSource.setCacheSeconds(10)
            tempMessageSource.setDefaultEncoding("UTF-8")
//            tempMessageSource.setUseCodeAsDefaultMessage(true)
            messageSource = tempMessageSource
        }

        return messageSource!!
    }

    @Bean
    fun localeResolver(): LocaleResolver? {
        val localeResolver = HeaderLocaleResolver()
        localeResolver.defaultLocale = Locale.Builder().setLanguage("vi").build()
        return localeResolver
    }

    @Bean
    fun localeChangeInterceptor(): LocaleChangeInterceptor {
        val lci = LocaleChangeInterceptor()
        lci.paramName = ""
        return lci
    }

    override fun addInterceptors(registry: InterceptorRegistry) {
        registry.addInterceptor(localeChangeInterceptor())
    }

//    @Bean("multipartResolver")
//    fun multipartResolver(): CommonsMultipartResolver {
//        val multipartResolver = CommonsMultipartResolver()
//        multipartResolver.setMaxUploadSize((25 * 1024 * 1024).toLong())
//        return multipartResolver
//    }


}