package vn.viso.shop.libs.rest

import vn.viso.shop.libs.rest.PaginationDto

class RestResponse (
        val data: Any? = null,
        val meta: PaginationDto<*>? = null,
        val message: String = "success",
        val status: Int = 1
)