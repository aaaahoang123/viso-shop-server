package vn.viso.shop.entity

import vn.viso.shop.common.enum.status.CommonStatus
import vn.viso.shop.entity.abstract.WithTimestampEntity
import javax.persistence.*

@Entity
@Table(name = "customer_types")
class CustomerType(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Int? = null,

        @Column(columnDefinition = "varchar(255) not null")
        var name: String? = null,

        @Column(columnDefinition = "tinyint default ${CommonStatus.ACTIVE}")
        var status: Int = CommonStatus.ACTIVE
): WithTimestampEntity()