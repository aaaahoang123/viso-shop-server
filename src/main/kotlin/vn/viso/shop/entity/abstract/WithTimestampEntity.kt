package vn.viso.shop.entity.abstract

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.MappedSuperclass

@MappedSuperclass
abstract class WithTimestampEntity {
    @Column(updatable = false, columnDefinition = "timestamp null")
    @CreationTimestamp
    open var createdAt: LocalDateTime? = null

    @UpdateTimestamp
    @Column(columnDefinition = "timestamp null")
    open var updatedAt: LocalDateTime? = null
}