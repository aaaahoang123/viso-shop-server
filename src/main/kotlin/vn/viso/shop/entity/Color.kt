package vn.viso.shop.entity

import vn.viso.shop.common.enum.status.CommonStatus
import vn.viso.shop.entity.abstract.WithTimestampEntity
import javax.persistence.*

@Entity
@Table(name = "colors")
open class Color(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    open var id: Int? = null,

    @Column(columnDefinition = "varchar(255) not null")
    open var name: String? = null,
    @Column(columnDefinition = "varchar(255) not null")
    open var code: String? = null,
    @Column(columnDefinition = "int default 0")
    open var sortNumber: Int = 0,

    @Column(columnDefinition = "tinyint default ${CommonStatus.ACTIVE}")
    open var status: Int = CommonStatus.ACTIVE
): WithTimestampEntity()