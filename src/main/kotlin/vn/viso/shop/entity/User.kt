package vn.viso.shop.entity

import vn.viso.shop.common.enum.status.CommonStatus
import vn.viso.shop.entity.abstract.WithTimestampEntity
import javax.persistence.*

@Entity
@Table(name = "users", uniqueConstraints = [UniqueConstraint(name = "email_unique", columnNames = ["email"])])
class User(
        @Id
        @Column(length = 50, nullable = false)
        var id: String? = null,
        @Column(length = 191, nullable = false)
        var email: String = "",
        var password: String? = null,
        var name: String? = null,
        var phoneNumber: String? = null,
        var address: String? = null,
        var avatar: String? = null,
        var birthday: Long? = null,
        var gender: Int? = null,
        var status: Int = CommonStatus.ACTIVE

//        @ManyToOne(cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
//        @JoinColumn(name = "policyId", columnDefinition = "int default 0")
//        var policy: UserPolicy? = null,
//        @Column(insertable = false, updatable = false)
//        var policyId: Int? = null
): WithTimestampEntity() {
//        @OneToMany(mappedBy = "createdBy", fetch = FetchType.LAZY)
//        var orders: Set<Order> = mutableSetOf()
}
