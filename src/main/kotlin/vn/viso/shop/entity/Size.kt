package vn.viso.shop.entity

import org.hibernate.annotations.Where
import vn.viso.shop.common.enum.status.CommonStatus
import vn.viso.shop.entity.abstract.WithTimestampEntity
import javax.persistence.*

@Entity
@Table(name = "sizes")
open class Size(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        open var id: Int? = null,

        @Column(columnDefinition = "varchar(255) not null")
        open var name: String? = null,

        @Column(columnDefinition = "tinyint not null default ${CommonStatus.ACTIVE}")
        open var status: Int = CommonStatus.ACTIVE,

        @Column(columnDefinition = "int not null default 0")
        open var sortNumber: Int = 0,

        @Column(insertable = false, updatable = false)
        open var parentId: Int? = null
) : WithTimestampEntity() {
        @ManyToOne(fetch = FetchType.LAZY, cascade = [CascadeType.DETACH])
        @JoinColumn(name = "parentId")
        open var parent: Size? = null

        @OneToMany(fetch = FetchType.LAZY, mappedBy = "parent")
        @Where(clause = "status = ${CommonStatus.ACTIVE}")
        @OrderBy("sortNumber desc")
        open var children: MutableSet<Size> = mutableSetOf()
}