package vn.viso.shop.entity

import org.hibernate.annotations.Where
import vn.viso.shop.common.enum.status.CommonStatus
import vn.viso.shop.entity.abstract.WithTimestampEntity
import javax.persistence.*

@Entity
@Table(name = "categories")
open class Category(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        open var id: Int? = null,

        @Column(columnDefinition = "varchar(191) not null")
        open var name: String? = null,

        @Column(columnDefinition = "varchar(191) not null", unique = true)
        open var slug: String? = null,

        open var icon: String? = null,

        @Column(columnDefinition = "int default 0")
        open var sortNumber: Int = 0,

        @Column(columnDefinition = "tinyint default " + CommonStatus.ACTIVE)
        open var status: Int = CommonStatus.ACTIVE,

        @Column(insertable = false, updatable = false)
        open var parentId: Int? = null
): WithTimestampEntity() {
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "parent")
    @Where(clause = "status = " + CommonStatus.ACTIVE)
    @OrderBy("sortNumber desc")
    open var children: MutableSet<Category> = mutableSetOf()

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Category::class, cascade = [CascadeType.DETACH])
    @JoinColumn(name = "parentId")
    open var parent: Category? = null
}