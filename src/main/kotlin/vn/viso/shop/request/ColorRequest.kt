package vn.viso.shop.request

import org.hibernate.validator.constraints.Length
import javax.validation.constraints.NotEmpty

class ColorRequest(
    @field:NotEmpty
    @field:Length(min = 1, max = 255)
    var name: String? = null,
    @NotEmpty
    @Length(min = 1, max = 255)
    var code: String? = null,

    var sortNumber: Int? = null,
    var status: Int? = null
)