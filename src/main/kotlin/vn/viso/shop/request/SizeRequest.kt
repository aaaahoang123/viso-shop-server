package vn.viso.shop.request

import org.hibernate.validator.constraints.Length
import javax.validation.constraints.NotEmpty

class SizeRequest(
        @field:NotEmpty
        @field:Length(min = 1, max = 255)
        var name: String? = null,
        var status: Int? = null,
        var sortNumber: Int? = null,
        var parentId: Int? = null
)