package vn.viso.shop.request

import org.hibernate.validator.constraints.Length
import javax.validation.constraints.NotEmpty

class CustomerTypeRequest(
    @field:NotEmpty
    @field:Length(min = 2, max = 50)
    var name: String? = null,
    var status: Int? = null
)