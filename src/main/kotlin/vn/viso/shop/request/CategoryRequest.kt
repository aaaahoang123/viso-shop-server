package vn.viso.shop.request

import org.hibernate.validator.constraints.Length
import javax.validation.constraints.Min
import javax.validation.constraints.NotEmpty
import kotlin.math.max

//'name' => ['required', 'string', 'max:150'],
//'parent_id' => ['nullable', 'numeric', 'exists:categories,id'],
//'icon' => ['nullable', 'string', 'max:255'],
//'status' => ['nullable', 'numeric', Rule::in(CommonStatus::getValues())],
//'size_id' => ['nullable', 'numeric', 'exists:sizes,id'],
//'sort_number' => ['nullable', 'numeric', 'min:0']
class CategoryRequest(
    @field:NotEmpty
    @field:Length(max = 150)
    var name: String?,
    var parentId: Int?,
    var icon: String?,
    var status: Int?,
    @field:Min(0)
    var sortNumber: Int?
)