package vn.viso.shop.request

import org.hibernate.validator.constraints.Length
import javax.validation.constraints.Email
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

class AuthRequest(
        @field:NotEmpty
        @field:NotNull
        @field:Email
        @field:Length(min = 4, max = 191)
        var email: String?,

        @field:NotEmpty
        @field:NotNull
        @field:Length(min = 6, max = 50)
        var password: String?
)