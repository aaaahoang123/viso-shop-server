package vn.viso.shop.repository.size

import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import vn.viso.shop.common.enum.status.CommonStatus
import vn.viso.shop.common.repository.BaseRepository
import vn.viso.shop.entity.Size

@Repository
interface SizeRepository : BaseRepository<Size, Int> {
//    @Query("select distinct vc from VehicleCategory vc join fetch vc.vehicles v where vc.id = :id and vc.status = :status and v.status = :status")
//    fun getByIdAndJoin(@Param("id") id: Int, @Param("status") status: Int = CommonStatus.ACTIVE.value): Optional<VehicleCategory>
    @Query("select distinct s from Size s left join fetch s.children c  where s.parentId is null and s.status = :status order by s.sortNumber desc, s.id asc")
    fun findAllSizesJoinChildren(@Param("status") status: Int = CommonStatus.ACTIVE): List<Size>
}