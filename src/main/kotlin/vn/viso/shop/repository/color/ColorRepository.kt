package vn.viso.shop.repository.color

import org.springframework.stereotype.Repository
import vn.viso.shop.common.repository.BaseRepository
import vn.viso.shop.entity.Color

@Repository
interface ColorRepository : BaseRepository<Color, Int>