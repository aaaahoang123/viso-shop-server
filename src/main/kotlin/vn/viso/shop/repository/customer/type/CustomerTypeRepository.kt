package vn.viso.shop.repository.customer.type

import org.springframework.stereotype.Repository
import vn.viso.shop.common.repository.BaseRepository
import vn.viso.shop.entity.CustomerType

@Repository
interface CustomerTypeRepository : BaseRepository<CustomerType, Int>