package vn.viso.shop.repository.user

import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import vn.viso.shop.common.repository.BaseRepository
import vn.viso.shop.entity.User
import java.util.*

@Repository
interface UserRepository: BaseRepository<User, String> {
    fun findMemberByEmail(email: String): Optional<User>
//    fun findById(id: String): User

//    @Query("select m from User m LEFT JOIN FETCH m.policy p LEFT JOIN FETCH p.roles where m.email = :email")
//    fun findMemberByEmailAndFetchPolicy(@Param("email") email: String): User
//
//    @Query("select m from User m LEFT JOIN FETCH m.policy p LEFT JOIN FETCH p.roles where m.id = :id")
//    fun findMemberByIdAndFetchPolicy(@Param("id") id: Int): Optional<User>
//
//    fun findUsersByPolicyIdAndStatus(policyId: Int, status: Int = CommonStatus.ACTIVE.value): List<User>
}
