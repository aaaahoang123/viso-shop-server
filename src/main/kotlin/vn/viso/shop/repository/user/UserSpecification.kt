package vn.viso.shop.repository.user

import org.springframework.data.jpa.domain.Specification
import vn.viso.shop.entity.User

fun userEmailEqual(email: String): Specification<User> {
    return Specification { root, _, criteriaBuilder ->
        criteriaBuilder.equal(root.get<String>("email"), email)
    }
}