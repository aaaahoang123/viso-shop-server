package vn.viso.shop.repository.category

import org.springframework.stereotype.Repository
import vn.viso.shop.common.enum.status.CommonStatus
import vn.viso.shop.common.repository.BaseRepository
import vn.viso.shop.entity.Category

@Repository
interface CategoryRepository: BaseRepository<Category, Int> {
    fun existsBySlug(slug: String): Boolean

    fun findAllByParentIdAndStatusOrderBySortNumberDesc(parentId: Int? = null, status: Int = CommonStatus.ACTIVE): List<Category>
    fun findAllByParentIdInAndStatusOrderBySortNumberDesc(parentIds: List<Int> = listOf(), status: Int = CommonStatus.ACTIVE): List<Category>
}