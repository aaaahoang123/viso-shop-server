package vn.viso.shop.service.customer.type

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.jpa.domain.Specification
import org.springframework.stereotype.Service
import vn.viso.shop.common.enum.status.CommonStatus
import vn.viso.shop.common.specification.hasStatus
import vn.viso.shop.common.utils.assignObject
import vn.viso.shop.entity.CustomerType
import vn.viso.shop.repository.customer.type.CustomerTypeRepository
import vn.viso.shop.request.CustomerTypeRequest

@Service
class CustomerTypeServiceImpl @Autowired constructor(
        private val customerTypeRepo: CustomerTypeRepository
) : CustomerTypeService {
    override fun create(data: CustomerTypeRequest): CustomerType {
        val type = assignObject(CustomerType(), data)
        return customerTypeRepo.save(type)
    }

    override fun list(): List<CustomerType> {
        return customerTypeRepo.findAll(
                Specification.where(hasStatus())
        )
    }

    override fun single(id: Int): CustomerType {
        return customerTypeRepo.findById(id).get()
    }

    override fun edit(id: Int, data: CustomerTypeRequest): CustomerType {
        val type = assignObject(single(id), data)
        return customerTypeRepo.save(type)
    }

    override fun delete(id: Int): CustomerType {
        val type = single(id);
        type.status = CommonStatus.INACTIVE
        return customerTypeRepo.save(type)
    }
}