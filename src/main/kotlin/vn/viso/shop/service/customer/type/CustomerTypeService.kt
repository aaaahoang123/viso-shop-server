package vn.viso.shop.service.customer.type

import vn.viso.shop.entity.CustomerType
import vn.viso.shop.request.CustomerTypeRequest

interface CustomerTypeService {
    fun create(data: CustomerTypeRequest): CustomerType

    fun list(): List<CustomerType>

    @Throws(exceptionClasses = [NoSuchElementException::class])
    fun single(id: Int): CustomerType

    @Throws(exceptionClasses = [NoSuchElementException::class])
    fun edit(id: Int, data: CustomerTypeRequest): CustomerType

    @Throws(exceptionClasses = [NoSuchElementException::class])
    fun delete(id: Int): CustomerType
}