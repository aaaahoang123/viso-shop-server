package vn.viso.shop.service.size

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import vn.viso.shop.common.enum.status.CommonStatus
import vn.viso.shop.common.utils.assignObject
import vn.viso.shop.entity.Size
import vn.viso.shop.libs.exception.ExecuteException
import vn.viso.shop.repository.size.SizeRepository
import vn.viso.shop.request.SizeRequest

@Service
class SizeServiceImpl @Autowired constructor(
        private val sizeRepo: SizeRepository
) : SizeService {
    override fun create(data: SizeRequest): Size {
        val size = assignObject(Size(), data)

        data.parentId?.run {
            val parent = isValidParentSize(single(this))
            size.parent = parent
        }

        return sizeRepo.save(size)
    }

    override fun list(): List<Size> {
        return sizeRepo.findAllSizesJoinChildren()
    }

    override fun single(id: Int): Size {
        return sizeRepo.findById(id).get()
    }

    override fun edit(id: Int, data: SizeRequest): Size {
        val size = single(id)
        if (size.parentId != data.parentId) {
            when (data.parentId) {
                null -> size.parent = null
                size.id -> throw ExecuteException("can_not_be_parent_of_self")
                else -> size.parent = isValidParentSize(single(data.parentId!!))
            }
        }
        val newSize = assignObject(size, data)
        newSize.parentId = data.parentId
        return sizeRepo.save(newSize)
    }

    override fun delete(id: Int): Size {
        val size = single(id)
        size.status = CommonStatus.INACTIVE
        return sizeRepo.save(size)
    }

    @Throws(exceptionClasses = [ExecuteException::class])
    private fun isValidParentSize(parent: Size): Size {
        if (parent.parentId != null) {
            throw ExecuteException("not_valid_parent_size")
        }
        return parent
    }
}