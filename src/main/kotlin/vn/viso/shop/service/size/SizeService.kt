package vn.viso.shop.service.size

import vn.viso.shop.entity.Size
import vn.viso.shop.libs.exception.ExecuteException
import vn.viso.shop.request.SizeRequest

interface SizeService {
    @Throws(exceptionClasses = [ExecuteException::class, NoSuchElementException::class])
    fun create(data: SizeRequest): Size

    fun list(): List<Size>

    @Throws(exceptionClasses = [NoSuchElementException::class])
    fun single(id: Int): Size

    @Throws(exceptionClasses = [ExecuteException::class, NoSuchElementException::class])
    fun edit(id: Int, data: SizeRequest): Size

    @Throws(exceptionClasses = [NoSuchElementException::class])
    fun delete(id: Int): Size
}