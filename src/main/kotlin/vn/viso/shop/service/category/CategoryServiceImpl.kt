package vn.viso.shop.service.category

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import vn.viso.shop.common.enum.status.CommonStatus
import vn.viso.shop.common.utils.assignObject
import vn.viso.shop.common.utils.toSlug
import vn.viso.shop.entity.Category
import vn.viso.shop.libs.exception.ExecuteException
import vn.viso.shop.repository.category.CategoryRepository
import vn.viso.shop.request.CategoryRequest

@Service
class CategoryServiceImpl @Autowired constructor(
        private val categoryRepo: CategoryRepository
) : CategoryService {

    override fun create(data: CategoryRequest): Category {
        val category = assignObject(Category(), data)
        if (data.parentId != null && data.parentId!! > 0) {
            val parent = single(data.parentId!!)
            category.parent = parent
        }
        category.slug = createSlug(data.name!!)
        return categoryRepo.save(category)
    }

    override fun list(): List<Category> {
        val parents = categoryRepo.findAllByParentIdAndStatusOrderBySortNumberDesc()

        var tempParents = parents

        do {
            val children = findAllChildrenByParents(tempParents)
            mapChildrenToParents(children, tempParents)
            tempParents = children
        } while(children.isNotEmpty())
        return parents
    }

    override fun single(id: Int): Category {
        return categoryRepo.findById(id).get()
    }

    override fun edit(id: Int, data: CategoryRequest): Category {
        var category = single(id)
        val oldParentId = category.parentId
        val oldName = category.name
        category = assignObject(category, data)

        if (oldParentId != data.parentId) {
            if (data.parentId == category.id) {
                throw ExecuteException("can_not_be_parent_of_self")
            }
            category.parent = data.parentId?.let { single(it) } ?: run { category.parentId = null; null }
        }
        if (oldName != data.name) {
            category.slug = createSlug(data.name!!)
        }
        return categoryRepo.save(category)
    }

    override fun delete(id: Int): Category {
        val category = single(id)
        category.status = CommonStatus.INACTIVE

        return categoryRepo.save(category)
    }

    private fun createSlug(name: String): String {
        val slug = toSlug(name)
        return if (categoryRepo.existsBySlug(slug))
            slug + "-" + System.currentTimeMillis()
        else
            slug
    }

    private fun findAllChildrenByParents(parents: List<Category>): List<Category> {
        return categoryRepo.findAllByParentIdInAndStatusOrderBySortNumberDesc(
            parents.map { it.id!! }
        )
    }

    private fun mapChildrenToParents(children: List<Category>, parents: List<Category>) {
        if (children.isEmpty()) {
            return
        }
        val childrenMap: MutableMap<Int, MutableSet<Category>> = mutableMapOf()

        for (child in children) {
            if (!childrenMap.containsKey(child.parentId)) {
                childrenMap[child.parentId!!] = mutableSetOf()
            }
            childrenMap[child.parentId!!]?.add(child)
        }
        for (parent in parents) {
            if (childrenMap.containsKey(parent.id)) {
                parent.children = childrenMap[parent.id!!]!!
            }
        }
    }
}