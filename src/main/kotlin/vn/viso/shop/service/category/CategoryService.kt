package vn.viso.shop.service.category

import vn.viso.shop.entity.Category
import vn.viso.shop.libs.exception.ExecuteException
import vn.viso.shop.request.CategoryRequest

interface CategoryService {
    @Throws(exceptionClasses = [ExecuteException::class])
    fun create(data: CategoryRequest): Category

    fun list(): List<Category>

    @Throws(exceptionClasses = [NoSuchElementException::class])
    fun single(id: Int): Category

    @Throws(exceptionClasses = [NoSuchElementException::class, ExecuteException::class])
    fun edit(id: Int, data: CategoryRequest): Category

    @Throws(exceptionClasses = [NoSuchElementException::class])
    fun delete(id: Int): Category
}