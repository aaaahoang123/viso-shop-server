package vn.viso.shop.service.color

import vn.viso.shop.entity.Color
import vn.viso.shop.libs.exception.ExecuteException
import vn.viso.shop.request.ColorRequest

interface ColorService {
    @Throws(exceptionClasses = [ExecuteException::class])
    fun create(req: ColorRequest): Color

    fun list(): List<Color>

    @Throws(exceptionClasses = [NoSuchElementException::class])
    fun single(id: Int): Color

    @Throws(exceptionClasses = [ExecuteException::class])
    fun edit(id: Int, req: ColorRequest): Color

    @Throws(exceptionClasses = [NoSuchElementException::class])
    fun delete(id: Int): Color
}