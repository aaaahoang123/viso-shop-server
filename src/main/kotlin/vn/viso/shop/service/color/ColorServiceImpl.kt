package vn.viso.shop.service.color

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.jpa.domain.Specification
import org.springframework.stereotype.Service
import vn.viso.shop.common.enum.status.CommonStatus
import vn.viso.shop.common.enum.type.OrderByType
import vn.viso.shop.common.specification.hasStatus
import vn.viso.shop.common.specification.orderBy
import vn.viso.shop.common.utils.assignObject
import vn.viso.shop.entity.Color
import vn.viso.shop.repository.color.ColorRepository
import vn.viso.shop.request.ColorRequest

@Service
class ColorServiceImpl @Autowired constructor(
        private val colorRepository: ColorRepository
) : ColorService {
    override fun create(req: ColorRequest): Color {
        val color = assignObject(Color(), req)
        return colorRepository.save(color)
    }

    override fun list(): List<Color> {
        val spec = Specification.where(
                orderBy<Color>(mapOf(
                        "sortNumber" to OrderByType.DESC,
                        "id" to OrderByType.ASC
                ))
        )
                ?.and(hasStatus())

        return colorRepository.findAll(spec)
    }

    override fun single(id: Int): Color {
        return colorRepository.findById(id).get()
    }

    override fun edit(id: Int, req: ColorRequest): Color {
        var color = single(id)
        color = assignObject(color, req)
        return colorRepository.save(color)
    }

    override fun delete(id: Int): Color {
        val color = single(id)
        color.status = CommonStatus.INACTIVE
        return colorRepository.save(color)
    }
}