package vn.viso.shop.service.dto

import org.hibernate.Hibernate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import vn.viso.shop.common.datetime.COMMON_DATETIME_FORMATTER
import vn.viso.shop.common.enum.status.CommonStatus
import vn.viso.shop.common.enum.type.Gender
import vn.viso.shop.entity.*
import vn.viso.shop.libs.locale.LocaleService

@Service
class DtoBuilderImpl @Autowired constructor(
     private val localeService: LocaleService
): DtoBuilder {
    override fun buildUserDto(user: User): Map<String, Any?> {
        val result = mutableMapOf<String, Any?>(
            "id" to user.id,
            "email" to user.email,
            "birthday" to user.birthday,
            "name" to user.name,
            "phoneNumber" to user.phoneNumber,
            "address" to user.address,
            "gender" to user.gender,
            "genderTitle" to if (user.gender == null) null else localeService.getMessage(user.gender!!, Gender.javaClass),
            "createdAt" to user.createdAt?.format(COMMON_DATETIME_FORMATTER),
            "updatedAt" to user.updatedAt?.format(COMMON_DATETIME_FORMATTER),
            "avatar" to user.avatar,
            "status" to user.status,
            "statusTitle" to localeService.getMessage(user.status, CommonStatus.javaClass)
        )
        return result
    }

    override fun buildCategoryDto(category: Category): Map<String, Any?> {
        val result = mutableMapOf<String, Any?>(
            "id" to category.id,
            "name" to category.name,
            "slug" to category.slug,
            "icon" to category.icon,
            "parentId" to category.parentId,
            "createdAt" to category.createdAt?.format(COMMON_DATETIME_FORMATTER),
            "updatedAt" to category.updatedAt?.format(COMMON_DATETIME_FORMATTER),
            "status" to category.status,
            "sortNumber" to category.sortNumber,
            "statusTitle" to localeService.getMessage(category.status, CommonStatus.javaClass)
        )
        if (Hibernate.isInitialized(category.parent)) {
            result["parent"] = category.parent?.let { it -> buildCategoryDto(it) }
        }

        if (Hibernate.isInitialized(category.children)) {
            result["children"] = category.children.map { buildCategoryDto(it) }
        }

        return result
    }

    override fun buildColorDto(color: Color): Map<String, Any?> {
        return mapOf(
            "id" to color.id,
            "name" to color.name,
            "code" to color.code,
            "status" to color.status,
            "createdAt" to color.createdAt?.format(COMMON_DATETIME_FORMATTER),
            "updatedAt" to color.updatedAt?.format(COMMON_DATETIME_FORMATTER),
            "sortNumber" to color.sortNumber,
            "statusTitle" to localeService.getMessage(color.status, CommonStatus.javaClass)
        )
    }

    override fun buildSizeDto(size: Size): Map<String, Any?> {
        val result = mutableMapOf<String, Any?>(
                "id" to size.id,
                "name" to size.name,
                "parentId" to size.parentId,
                "status" to size.status,
                "sortNumber" to size.sortNumber,
                "createdAt" to  size.createdAt?.format(COMMON_DATETIME_FORMATTER),
                "updatedAt" to size.updatedAt?.format(COMMON_DATETIME_FORMATTER),
                "statusTitle" to localeService.getMessage(size.status, CommonStatus.javaClass)
        )

//
//        if (Hibernate.isInitialized(size.parent)) {
//            result["parent"] = size.parent?.let {  buildSizeDto(it) }
//        }

        if (Hibernate.isInitialized(size.children)) {
            result["children"] = size.children.map { buildSizeDto(it) }
        }

        return result
    }


    override fun buildCustomerTypeDto(type: CustomerType): Map<String, Any?> {
        return mapOf(
                "id" to type.id,
                "name" to type.name,
                "status" to type.status,
                "statusTitle" to localeService.getMessage(type.status, CommonStatus.javaClass),
                "createdAt" to type.createdAt?.format(COMMON_DATETIME_FORMATTER),
                "updatedAt" to type.updatedAt?.format(COMMON_DATETIME_FORMATTER)
        )
    }
}