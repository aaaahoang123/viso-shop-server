package vn.viso.shop.service.dto

import vn.viso.shop.entity.*

interface DtoBuilder {
    fun buildUserDto(user: User): Map<String, Any?>
    fun buildCategoryDto(category: Category): Map<String, Any?>
    fun buildColorDto(color: Color): Map<String, Any?>
    fun buildSizeDto(size: Size): Map<String, Any?>
    fun buildCustomerTypeDto(type: CustomerType): Map<String, Any?>
}