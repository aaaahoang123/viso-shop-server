package vn.viso.shop.controller.shared

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import vn.viso.shop.API_PREFIX
import vn.viso.shop.entity.User
import vn.viso.shop.libs.auth.ReqUser
import vn.viso.shop.service.dto.DtoBuilder
import javax.servlet.http.HttpServletRequest

@RestController
@RequestMapping("$API_PREFIX/v1/users")
class UserController @Autowired constructor(
    private val dtoBuilder: DtoBuilder
) {
    @GetMapping("/my-info")
    fun myInfo(@ReqUser user: User, req: HttpServletRequest): Map<String, Any> {
        return mapOf(
            "user" to dtoBuilder.buildUserDto(user),
            "token" to req.getHeader(HttpHeaders.AUTHORIZATION).replace("Bearer ", "")
        )
    }

}