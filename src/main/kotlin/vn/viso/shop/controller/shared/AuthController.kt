package vn.viso.shop.controller.shared

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import vn.viso.shop.libs.auth.AuthService
import vn.viso.shop.request.AuthRequest
import vn.viso.shop.service.dto.DtoBuilder

import javax.validation.Valid

@RestController
@RequestMapping(value = ["/api/auth"])
@CrossOrigin
class AuthController @Autowired constructor(
        private val authService: AuthService,
        private val dtoBuilder: DtoBuilder
) {

    @PostMapping(value = ["/register"])
    fun register(@RequestBody @Valid registerData: AuthRequest): Map<String, Any> {
        val user = authService.register(registerData)
        val token = authService.generateToken(user)
        return mapOf(
            "user" to dtoBuilder.buildUserDto(user),
            "token" to token
        )
    }

    @PostMapping(value = ["/login"])
    fun login(@RequestBody @Valid loginData: AuthRequest): Map<String, Any> {
        val user = authService.attemptMember(loginData.email!!, loginData.password!!)
        val token = authService.generateToken(user)
        return mapOf(
            "user" to dtoBuilder.buildUserDto(user),
            "token" to token
        )
    }
}
