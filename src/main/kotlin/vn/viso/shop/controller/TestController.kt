package vn.viso.shop.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import vn.viso.shop.libs.exception.ExecuteException

@RestController
@RequestMapping(value = ["/api/v1"])
class TestController @Autowired constructor(
) {
    @GetMapping("/cax")
    fun get(): ResponseEntity<Any> {
        throw ExecuteException("failed")
    }
}