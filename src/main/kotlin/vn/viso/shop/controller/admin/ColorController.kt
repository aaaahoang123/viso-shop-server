package vn.viso.shop.controller.admin

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import vn.viso.shop.API_PREFIX
import vn.viso.shop.request.ColorRequest
import vn.viso.shop.service.color.ColorService
import vn.viso.shop.service.dto.DtoBuilder
import javax.validation.Valid

@RestController
@RequestMapping("$API_PREFIX/admin/colors")
class ColorController @Autowired constructor(
    private val colorService: ColorService,
    private val dtoBuilder: DtoBuilder
) {
    @PostMapping
    fun create(@RequestBody @Valid req: ColorRequest): Map<String, Any?> {
        return dtoBuilder.buildColorDto(
            colorService.create(req)
        )
    }

    @GetMapping
    fun list(): List<Map<String, Any?>> {
        return colorService.list().map { dtoBuilder.buildColorDto(it) }
    }

    @GetMapping("/{id}")
    fun single(@PathVariable id: Int): Map<String, Any?> {
        return dtoBuilder.buildColorDto(
                colorService.single(id)
        )
    }

    @PutMapping("/{id}")
    fun edit(@PathVariable id: Int, @RequestBody @Valid req: ColorRequest): Map<String, Any?> {
        return dtoBuilder.buildColorDto(
                colorService.edit(id, req)
        )
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Int): Map<String, Any?> {
        return dtoBuilder.buildColorDto(
                colorService.delete(id)
        )
    }
}