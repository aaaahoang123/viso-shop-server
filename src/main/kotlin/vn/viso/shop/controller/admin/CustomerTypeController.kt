package vn.viso.shop.controller.admin

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import vn.viso.shop.API_PREFIX
import vn.viso.shop.request.CustomerTypeRequest
import vn.viso.shop.service.customer.type.CustomerTypeService
import vn.viso.shop.service.dto.DtoBuilder
import javax.validation.Valid

@RestController
@RequestMapping("$API_PREFIX/admin/customer-types")
class CustomerTypeController @Autowired constructor(
        private val customerTypeService: CustomerTypeService,
        private val dtoBuilder: DtoBuilder
) {

    @PostMapping
    fun create(@RequestBody @Valid req: CustomerTypeRequest): Map<String, Any?> {
        return dtoBuilder.buildCustomerTypeDto(
                customerTypeService.create(req)
        )
    }

    @GetMapping
    fun list(): List<Map<String, Any?>> {
        return customerTypeService.list().map { dtoBuilder.buildCustomerTypeDto(it) }
    }

    @GetMapping("/{id}")
    fun single(@PathVariable id: Int): Map<String, Any?> {
        return dtoBuilder.buildCustomerTypeDto(
                customerTypeService.single(id)
        )
    }

    @PutMapping("/{id}")
    fun edit(@PathVariable id: Int, @RequestBody @Valid req: CustomerTypeRequest): Map<String, Any?> {
        return dtoBuilder.buildCustomerTypeDto(
                customerTypeService.edit(id, req)
        )
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Int): Map<String, Any?> {
        return dtoBuilder.buildCustomerTypeDto(
                customerTypeService.delete(id)
        )
    }
}