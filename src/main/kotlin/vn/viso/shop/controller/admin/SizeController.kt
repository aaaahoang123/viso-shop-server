package vn.viso.shop.controller.admin

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import vn.viso.shop.API_PREFIX
import vn.viso.shop.request.SizeRequest
import vn.viso.shop.service.dto.DtoBuilder
import vn.viso.shop.service.size.SizeService
import javax.validation.Valid

@RestController
@RequestMapping("$API_PREFIX/admin/sizes")
class SizeController @Autowired constructor(
        private val sizeService: SizeService,
        private val dtoBuilder: DtoBuilder
) {

    @PostMapping
    fun create(@RequestBody @Valid data: SizeRequest): Map<String, Any?> {
        return dtoBuilder.buildSizeDto(
                sizeService.create(data)
        )
    }

    @GetMapping
    fun list(): List<Map<String, Any?>> {
        return sizeService.list()
                .map { dtoBuilder.buildSizeDto(it) }
    }

    @GetMapping("/{id}")
    fun single(@PathVariable id: Int): Map<String, Any?> {
        return dtoBuilder.buildSizeDto(
                sizeService.single(id)
        )
    }

    @PutMapping("/{id}")
    fun edit(@PathVariable id: Int, @RequestBody @Valid data: SizeRequest): Map<String, Any?> {
        return dtoBuilder.buildSizeDto(
                sizeService.edit(id, data)
        )
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Int): Map<String, Any?> {
        return dtoBuilder.buildSizeDto(
                sizeService.delete(id)
        )
    }
}