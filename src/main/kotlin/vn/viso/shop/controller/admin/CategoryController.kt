package vn.viso.shop.controller.admin

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import vn.viso.shop.API_PREFIX
import vn.viso.shop.request.CategoryRequest
import vn.viso.shop.service.category.CategoryService
import vn.viso.shop.service.dto.DtoBuilder
import javax.validation.Valid

@RestController
@RequestMapping("$API_PREFIX/admin/categories")
class CategoryController @Autowired constructor(
        private val categoryService: CategoryService,
        private val dtoBuilder: DtoBuilder
) {

    @PostMapping
    fun create(@RequestBody @Valid req: CategoryRequest): Map<String, Any?> {
        return dtoBuilder.buildCategoryDto(categoryService.create(req))
    }

    @GetMapping
    fun list(): List<Map<String, Any?>> {
        return categoryService.list().map { dtoBuilder.buildCategoryDto(it) }
    }

    @GetMapping("/{id}")
    fun single(@PathVariable id: Int): Map<String, Any?> {
        return dtoBuilder.buildCategoryDto(categoryService.single(id))
    }

    @PutMapping("/{id}")
    fun edit(@PathVariable id: Int, @RequestBody @Valid data: CategoryRequest): Map<String, Any?> {
        return dtoBuilder.buildCategoryDto(categoryService.edit(id, data))
    }

    @DeleteMapping("/{id}")
    fun edit(@PathVariable id: Int): Map<String, Any?> {
        return dtoBuilder.buildCategoryDto(categoryService.delete(id))
    }
}